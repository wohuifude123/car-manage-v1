import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Layout, Menu, Icon } from 'antd';
import style from '../styles/home-layout.less';
import PropTypes from 'prop-types';
import Home from '../pages/Home';
import PlatformManage from '../pages/PlatformManage';
import CarManage from '../pages/CarManage';
import VersionManage from '../pages/VersionManage';
import DispatchIssue from '../pages/DispatchIssue';
import TestEqManage from '../pages/TestEqManage';
import ProgressIssue from '../pages/ProgressIssue';
import EqManage from '../pages/EqManage';
import BookList from '../pages/BookList';
import AddBook from '../pages/AddBook';
import Device from '../components/device';
import TableMarker from '../components/TableMarker';
import displayPhoto from '../styles/images/displayPhoto.png';

const SubMenu = Menu.SubMenu;
const MenuItem = Menu.Item;

class HomeLayout extends React.Component {
  constructor(props) {
    super(props);

    const _self = this;
    this.newTabIndex = 0;
    const panes = [
      { title: '首页', content: 'default-style', key:'1' },
    ];
    const componentNames = [
      { title: '默认', content: 'default-style', key: '1' }
    ]
    _self.state = {
      activeKey: panes[0].key,
      panes,
      selectedKeys: "/app",
      collapsed: false, // 控制导航缩放
    };
    //this.removeComponent = this.removeComponent.bind(this);//推荐写法
  }


  static contextTypes = {
    router: PropTypes.object
  }

  onChange = (activeKey) => {
    this.setState({ activeKey });
  }

  onEdit = (targetKey, action) => {
    this[action](targetKey);
  }


  add = (event) => {
    //console.log('增加新的属性');
    const _self = this;
    const panes = this.state.panes;
    const activeKey = `newTab${this.newTabIndex++}`;
    let dataTitle = event.target.getAttribute("data-title");
    let dataContent = event.target.getAttribute("data-content");
    let dataIndex = event.target.getAttribute("data-index");
    let keys = {};
    //panes.push({ title: dataTitle, content: dataContent, key: activeKey });
    
    panes.map( (item, index)=> {
        //console.log( 'itemKey ==', item['key'] );
        keys[item['key']] = 1;
        //console.log( 'index ==', index );
    })

    console.log( 'keys ===', keys );
    console.log( 'keys[dataIndex] === ', keys[dataIndex]);
    if( keys[dataIndex] !== 1) {
      panes.push({ title: dataTitle, content: dataContent, key: dataIndex });
      _self.setState({
        panes: panes,
        activeKey: dataIndex
      });
    }
  }

  toggleCollapsed = () => {
    const _self = this;
    _self.setState({
      collapsed: !_self.state.collapsed,
    });
  }

  editTag = (activeKey) => {
    const _self = this;
    _self.setState({ activeKey });
  };

  removeTag = (targetKey) => {
      // 删除
      const _self = this;
      let activeKey = this.state.activeKey;
      console.log('点击移除组件 == ', targetKey);
      let lastIndex;
      console.log ( 'targetKey == ', targetKey);
      this.state.panes.forEach((pane, i) => {
        if (pane.key === targetKey) {
          lastIndex = i - 1;
        }
      });
      const panes = this.state.panes.filter(pane => pane.key !== targetKey);
      if (lastIndex >= 0 && activeKey === targetKey) {
        activeKey = panes[lastIndex].key;
      }
      console.log( 'panes == ', panes);
      this.setState({ 
        panes: panes, 
        activeKey: activeKey
      });
  }

  remove = (targetKey) => {
    if (targetKey === '/app') return;
    let activeKey = this.state.activeKey;
    let lastIndex;
    this.state.panes.forEach((pane, i) => {
      if (pane.key === targetKey) {
        lastIndex = i - 1;
      }
    });
    const panes = this.state.panes.filter(pane => pane.key !== targetKey);
    if (lastIndex >= 0 && activeKey === targetKey) {
      activeKey = panes[lastIndex].key;
    }
    this.setState({ 
      panes,
      activeKey,
      selectedKeys: activeKey
    });
  }

  handleClick({key}) {
    let hasKey = this.state.panes.filter((item)=> {
      return item.key === key;
    });
    if (hasKey.length === 0) {
      const arrays = {
        '/app': 'app',
        '/app_name': '首页',
        '/platformManage': 'platformManage',
        '/platformManage_name': '平台管理',
        '/carManage': 'carManage',
        '/carManage_name': '车行管理',
        '/testEqManage': 'testEqManage',
        '/testEqManage_name': '测试设备管理',
        '/eqManage': 'eqManage',
        '/eqManage_name': '设备管理',
        '/versionManage': 'versionManage',
        '/versionManage_name': '版本管理',
        '/dispatchIssue': 'dispatchIssue',
        '/dispatchIssue_name': '发布任务',
        '/progressIssue': 'progressIssue',
        '/progressIssue_name': '发布任务',
        '/booklist': 'booklist',
        '/booklist_name': '图书列表',
        '/addbook': 'addbook',
        '/addbook_name': '添加图书'
      }
      const panes = this.state.panes;
      const activeKey = key;
      panes.push({ title: arrays[`${key}_name`], content: arrays[key], key: activeKey });
      this.setState({ 
        panes,
        activeKey,
        selectedKeys: key});
    } else {
      this.setState({
        selectedKeys: key,
        activeKey: key
      });
    }

  }

  userLogin =(e)=> {
      console.log("用户登陆功能");
      let url='http://118.24.248.134:8089/admin/user/login';
      let postData = {
          "userName":"yukaifei",
          "pwd":"123456"
      };

      let dataStorage = window.localStorage;


      fetch("http://118.24.248.134:8089/admin/user/login",{
          method:"POST",
          mode: 'cors',
          headers: {
              'Content-Type': 'application/json'
          },
          body:JSON.stringify({
              "userName":"yukaifei",
              "pwd":"123456"
          })
      }).then(
          response => {
              // console.log("response ==", response);
              return response.json();
          }
      ).then((data) => {
          // console.log(data["data"]["userAdminToken"]);
          let userToken = data["data"]["userAdminToken"];
          // console.log(typeof(userToken));
          if (typeof userToken == 'object') {
              userToken = JSON.stringify(userToken);
              userToken = 'obj-' + userToken;
          } else {
              userToken = 'str-' + userToken;
          }
          console.log("userToken ==", userToken);
          dataStorage.setItem("userToken", userToken);
      });

  }

  handleTabClick(tab) {

    this.setState({selectedKeys: tab});
  }

  render () {
  	const _self = this;
  		return (
  			<div className={'main_container'}>
				<main className='main'>
					<div className='menu'>
						<div 
							className={'user_information'}
							style={ {height: _self.state.collapsed?75:135 }}
						>
							<div className="display_photo">
								<img 
									src={displayPhoto}
									style={{ width:48, height:48}}
								/>
							</div>
							{
								_self.state.collapsed ? '':
								<div className="info_container">
									<div className="name"></div>
									<div className="email"></div>
								</div>
							}
							

						</div>
						<Menu
							style={{ width: _self.state.collapsed? 90 : 230 }}
							defaultSelectedKeys={['1']}
				            defaultOpenKeys={['sub1']}
				            mode="inline"
				            theme="dark"
				            inlineCollapsed={_self.state.collapsed}
						>
							<MenuItem key={"/app"}>
                <Link to={"/users/add"}><Icon type="home" /><span>总体情况</span></Link>
              </MenuItem>
							<SubMenu key="ota-manage" title={<span><Icon type="user"/><span>OTA管理</span></span>}>
                <MenuItem key="platform-manage">
                  平台管理
								</MenuItem>
            		<MenuItem key="car-manage">
            			<div
                    onClick={_self.add}
                    data-title="车型管理"
                    data-content="car-manage"
                    data-index="car"
                  >
                    车型管理
                  </div>
            		</MenuItem>
                  <MenuItem key="device-manage">
                    <div
                      onClick={_self.add}
                      data-title="设备管理"
                      data-content="device-manage"
                      data-index='device'
                    >
                      设备管理
                    </div>
                  </MenuItem>

                                <MenuItem key="user-list">
                                    <div
                                        onClick={_self.add}
                                        data-title="用户列表"
                                        data-content="user-list"
                                        data-index="userList"
                                    >
                                        用户列表
                                    </div>
                                </MenuItem>

                                <MenuItem key="user-info">
                                    <div
                                        onClick={_self.add}
                                        data-title="用户详情"
                                        data-content="user-info"
                                        data-index="userInfo"
                                    >
                                        用户详情
                                    </div>
                                </MenuItem>

                                <MenuItem key="user-car-info">
                                    <div
                                        onClick={_self.add}
                                        data-title="用户详情"
                                        data-content="user-car-info"
                                        data-index="userCarInfo"
                                    >
                                        用户车辆信息
                                    </div>
                                </MenuItem>


                                <MenuItem key="car-list">
                                    <div
                                        onClick={_self.add}
                                        data-title="车辆列表"
                                        data-content="car-list"
                                        data-index='carList'
                                    >
                                        车辆列表
                                    </div>
                                </MenuItem>

                                <MenuItem key="log-list">
                                    <div
                                        onClick={_self.add}
                                        data-title="日志列表"
                                        data-content="log-list"
                                        data-index='logList'
                                    >
                                        日志列表
                                    </div>
                                </MenuItem>

                                <MenuItem key="admin-list">
                                    <div
                                        onClick={_self.add}
                                        data-title="管理列表"
                                        data-content="admin-list"
                                        data-index='adminList'
                                    >
                                        管理员列表
                                    </div>
                                </MenuItem>

                                <MenuItem key="add-admin">
                                    <div
                                        onClick={_self.add}
                                        data-title="管理列表"
                                        data-content="add-admin"
                                        data-index='addAdmin'
                                    >
                                        增加管理员
                                    </div>
                                </MenuItem>




                  <SubMenu key="introduce-manage" title="发布管理">
                    <MenuItem key="10">发布任务</MenuItem>
                    <MenuItem key="11">版本管理</MenuItem>
                    <MenuItem key="12">测试设备管理</MenuItem>
                  </SubMenu>
                  <SubMenu key="/countManage" title="统计管理">
                    <MenuItem key="/progressIssue">进度任务</MenuItem>
                  </SubMenu>
              </SubMenu>
              
              <SubMenu key="system-manage" title={<span><Icon type="book"/><span>系统管理</span></span>}>
                <MenuItem key="user-manage">
                  <div>
                      <Link to={'/user/add'}>用户管理</Link>
                  </div>
                </MenuItem>
                <MenuItem key="role-manage">
                  角色管理
                </MenuItem>
                <MenuItem key="menu-manage">
                  菜单管理
                </MenuItem>
                <MenuItem key="department-manage">
                  部门管理
                </MenuItem>
                <MenuItem key="position-manage">
                  岗位管理
                </MenuItem>
                <MenuItem key="dictionary-manage">
                  字典管理
                </MenuItem>
                <MenuItem key="parameter-manage">
                  参数设置
                </MenuItem>
                <MenuItem key="notice-manage">
                  通知公告
                </MenuItem>
                <SubMenu key="log-manage" title="日志管理">
                  <MenuItem key="operate-log">操作日志</MenuItem>
                  <MenuItem key="login-log">登录日志</MenuItem>
                </SubMenu>
                <MenuItem key="area-manage">
                  区域管理
                </MenuItem>
              </SubMenu>

              <SubMenu key="system-tool" title={<span><Icon type="book"/><span>系统工具</span></span>}>
                <MenuItem key="form-build">
                  <div onClick={_self.add}>表单构建</div>
                </MenuItem>
                <MenuItem key="code-product">
                  <div onClick={_self.add}>代码生成</div>
                </MenuItem>
                <MenuItem key="system-interface">
                  <div onClick={_self.add}>系统接口</div>
                </MenuItem>
              </SubMenu>
              <MenuItem key={"/app123"}>
                  <Link to={"/user/login"}><Icon type="home"/><span>用户登录</span></Link>
              </MenuItem>
            </Menu>
          </div>

          <div className='content'>
            <header>
                <div className={'collapseButtonContainer'}>
            		<Button
                        onClick={_self.toggleCollapsed}
                        className='collapseButton'
                        size={'large'}
	              	>
                        <Icon type={_self.state.collapsed ? 'menu-unfold' : 'menu-fold'} />
	              	</Button>
            	</div>
            	<div className={'companyName'}>
            		<div onClick={_self.userLogin}>心里只有你生活网后台管理系统</div>
            	</div>
            </header>
            <TableMarker
              componentNames={_self.state}
              removeTag={_self.removeTag}
              editTag={_self.editTag}
              activeKey={_self.state.activeKey}
            />
          </div>
        </main>
        <footer>
            心里只有你生活网
        </footer>
      </div>
    );
  }
}

export default HomeLayout;