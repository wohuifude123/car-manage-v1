import React from 'react';
import { Redirect, Link } from 'react-router-dom';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import '../styles/user-add-layout.less';
import Helper from '../utils/helper';
import PropTypes from 'prop-types';

const FormItem = Form.Item;

class UserAddLayout extends React.Component {

    constructor(props) {
        super(props);

        const _self = this;
        _self.state = {
            isLoginSuccess: false,
            sexValue: 0,
            isAddSuccess: {
                status: false,
                content:'开始添加新的用户'
            }
        };
        _self.handleSexChange = _self.handleSexChange.bind(this);
    }

    userLogin = () => {
        const _self = this;
        let phoneValue = Helper.stringTrim(_self.refs.phone.value);
        let passwordValue = Helper.stringTrim(_self.refs.password.value);
        let usernameValue = Helper.stringTrim(_self.refs.username.value);
        let sexValue = parseInt(_self.state.sexValue);
        let qqValue = Helper.stringTrim(_self.refs.qq.value);
        let weChatValue = Helper.stringTrim(_self.refs.weChat.value);

        // console.log('用户登陆功能 == ', phoneValue, passwordValue);


        let token = sessionStorage.getItem('token');
        let operator = sessionStorage.getItem('operator');

        // let url = 'http://127.0.0.1:6030/api/manage/user/add';
        let url='/adm/api/manage/temporary/user/add';

        let postData = {
            phone: phoneValue,
            password: passwordValue,
            username: usernameValue,
            sex: sexValue,
            qq: qqValue,
            weChat: weChatValue,
            token: token,
            operator: operator
        };

        console.log('postData == ', postData);

        let headers = {
            'Content-Type': 'application/json'
        };

        fetch(url, {
            method:'POST',
            mode: 'cors',
            // credentials: 'include',
            headers: headers,
            body: JSON.stringify(postData)
        }).then(
            response => {
                // console.log("response ==", response);
                return response.json();
            }
        ).then((data) => {
            // console.log('添加用户返回信息 == ', data);
            let isAddSuccess = {};
            if(data['code'] === '0001') {
                isAddSuccess = {
                    status: true,
                    content:'成功添加一个用户'
                };
                _self.setState({
                    isAddSuccess
                });
            } else if(data['code'] === '0002'){
                // console.log(data);
                isAddSuccess = {
                    status: true,
                    content: data.detail
                };
                _self.setState({
                    isAddSuccess
                });

            }
        });
    };

    handleSexChange = (e) => {
        const _self = this;
        _self.setState({
            sexValue: e.target.value
        });
    };

    inputPasswordFocus = () => {
        const _self = this;
        // console.log(_self.refs.password.value);
        _self.refs.password.value = '';
    };

    render() {
        const _self = this;
        if(_self.state.isLoginSuccess){
            console.log(this.state.isLoginSuccess);
            return <Redirect to={{ pathname: '/'}} />;
        }

        return (
            <div className="user-add">
                <div className="login-form" >
                    <div className="login-logo">
                        <div >增加新的用户</div>
                    </div>
                    <div className={'fl add-user-form'}>
                        <Form onSubmit={this.handleSubmit} style={{maxWidth: '300px'}}>
                            <div className={'phone-input-container'}>
                                <input
                                    className={'phone-input'}
                                    placeholder="手机号码"
                                    ref='phone'
                                >
                                </input>
                            </div>
                            <div className={'password-input-container'}>
                                <input
                                    className={'password-input'}
                                    placeholder="密码"
                                    ref='password'
                                    defaultValue={'123456'}
                                    onFocus={_self.inputPasswordFocus}
                                >
                                </input>
                            </div>

                            <div className={'username-input-container'}>
                                <input
                                    className={'username-input'}
                                    placeholder="用户姓名"
                                    ref='username'

                                >
                                </input>
                            </div>


                            <div className={'sex-input-container'}>
                                <div className={'fl man-select'}>
                                    <input
                                        type="radio"
                                        name='gender'
                                        value="0"
                                        defaultChecked
                                        onChange={_self.handleSexChange}
                                    />男
                                </div>
                                <div className={'fl woman-select'}>
                                    <input
                                        type="radio"
                                        name='gender'
                                        value="1"
                                        onChange={_self.handleSexChange}
                                    />
                                    女
                                </div>
                            </div>

                            <div className={'qq-input-container'}>
                                <input
                                    className={'qq-input'}
                                    placeholder="QQ"
                                    ref='qq'
                                >
                                </input>
                            </div>

                            <div className={'weChat-input-container'}>
                                <input
                                    className={'weChat-input'}
                                    placeholder="微信"
                                    ref='weChat'

                                >
                                </input>
                            </div>


                            <Button
                                type="primary"
                                className="login-form-button"
                                style={{width: '100%'}}
                                onClick ={this.userLogin}
                            >
                                添加
                            </Button>
                        </Form>
                    </div>

                    <div className={_self.state.isAddSuccess.status ? 'return-successMsg fl ' : 'return-message fl'}>
                        { _self.state.isAddSuccess.content }
                    </div>

                </div>
            </div>
        );
    }

    componentDidMount() {
        //
    }

    componentDidUpdate(prevProps) { // React 16.3+弃用componentWillReceiveProps
        //
    }
    handleSubmit = (e) => {
        e.preventDefault();
        //
    };
}

export default UserAddLayout;