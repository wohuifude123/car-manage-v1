import React from 'react';
import { Redirect, Link } from 'react-router-dom';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import style from '../styles/login-layout.less';
import PropTypes from 'prop-types';

const FormItem = Form.Item;

class LoginLayout extends React.Component {

    constructor(props) {
        super(props);

        const _self = this;
        _self.state = {
            isLoginSuccess: false
        };
    }

    userLogin = () => {
        const _self = this;
        let usernameValue = _self.refs.username.value;
        let passwordValue = _self.refs.password.value;

        // console.log('用户登陆功能 == ', usernameValue, passwordValue);

        let url='/adm/api/manage/login/phone';

        // let url='http://39.106.161.34:6030/api/manage/login/phone';

        let postData = {
            username: usernameValue,
            password: passwordValue
        };

        let headers = {
            'Content-Type': 'application/json'
        };

        fetch(url, {
            method: 'POST',
            mode: 'cors',
            // credentials: 'include',
            headers: headers,
            body: JSON.stringify(postData)
        }).then(
            response => {
                // console.log("response ==", response);
                return response.json();
            }
        ).then((data) => {
            console.log(data);
            if(data['code'] === '0001') {
                // _self.context.router.push('/');
                sessionStorage.setItem('token', data.token);
                sessionStorage.setItem('operator', data.adminId);
                _self.setState({isLoginSuccess: true});
            }
        });
    };

    render() {
        const _self = this;
        if(_self.state.isLoginSuccess){
            // console.log(this.state.isLoginSuccess);
            return <Redirect to={{ pathname: '/'}} />;
        }

        return (
            <div className="login">
                <div className="login-form" >
                    <div className="login-logo">
                        <div >后台管理系统登陆</div>
                    </div>
                    <Form onSubmit={this.handleSubmit} style={{maxWidth: '300px'}}>
                        <div className={'username-input-container'}>
                            <input
                                className={'username-input'}
                                placeholder="账号"
                                ref='username'
                            >
                            </input>
                        </div>
                        <div className={'password-input-container'}>
                            <input
                                type={'password'}
                                className={'password-input'}
                                placeholder="密码"
                                ref='password'
                            >
                            </input>
                        </div>

                        <Button
                            type="primary"
                            className="login-form-button"
                            style={{width: '100%'}}
                            onClick ={this.userLogin}
                        >
                            登录
                        </Button>
                    </Form>
                </div>
            </div>
        );
    }

    componentDidMount() {
        //
    }

    componentDidUpdate(prevProps) { // React 16.3+弃用componentWillReceiveProps
        //
    }
    handleSubmit = (e) => {
        e.preventDefault();
        //
    };
}

export default LoginLayout;