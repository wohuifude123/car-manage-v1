/**
 * 功能：第三方工具类
 * 作者：刘建
 * 时间：2018年7月23日
 */

const stringTrim =(data) => {
    return data.replace(/(^\s*)|(\s*$)/g, '');
};

module.exports = {
    stringTrim: stringTrim
};