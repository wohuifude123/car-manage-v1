import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.less'

/**
 * 作者：Abbott.liu
 * 时间：2018年2月21日
 * 描述：车联网 - 设备
 */

class CoordinateBar extends React.Component {
    constructor() {
        super();
        this.state = {
            name: [],
            quantity: []
        };
    }

    _init() {
        // 参数设置
        const _self = this;
        return import(/* webpackChunkName: "echarts" */ 'echarts').then(echarts => {
            let myChart = echarts.init(_self.refs.coordinateBar);
            // 指定图表的配置项和数据
            let option = {
                    angleAxis: {
                        type: 'category',
                        data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
                        z: 10
                    },
                    radiusAxis: {
                    },
                    polar: {
                    },
                    series: [{
                        type: 'bar',
                        data: [1, 2, 3, 4, 3, 5, 1],
                        coordinateSystem: 'polar',
                        name: 'A',
                        stack: 'a'
                    }, {
                        type: 'bar',
                        data: [2, 4, 6, 1, 3, 2, 1],
                        coordinateSystem: 'polar',
                        name: 'B',
                        stack: 'a'
                    }, {
                        type: 'bar',
                        data: [1, 2, 3, 4, 1, 2, 5],
                        coordinateSystem: 'polar',
                        name: 'C',
                        stack: 'a'
                    }],
                    legend: {
                        show: true,
                        data: ['A', 'B', 'C']
                    }
            };
            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);
        })
    }

    render() {

        return (
            <div className={'coordinateBar'} ref='coordinateBar'>
            123
            </div>
        );
    }

    componentDidMount(){
        const _self = this;
        _self._init();
    }

    // 组件接收到新的props时调用,并将其作为参数nextProps使用
    UNSAFE_componentWillReceiveProps(nextProps) {  // 接收新的参数
        //console.log('设备')
        //console.log(nextProps.data)
       
    }
    
    componentDidUpdate() {
        this._init();
    }

}

export default CoordinateBar;
