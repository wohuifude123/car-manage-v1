import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.less'
import { Icon } from 'antd';
import StackArea from '../StackArea';
import StackBar from '../StackBar';
import CoordinateBar from '../CoordinateBar';
import CoordinateStack from '../CoordinateStack';

/**
 * 作者：Abbott.liu
 * 时间：2018年2月21日
 * 描述：车联网 - 设备
 */

class CarBar extends React.Component {
    constructor() {
        super();
        this.state = {
            name: [],
            quantity: []
        }
    }

    _init() {
        // 参数设置
        const _self = this;
        return import(/* webpackChunkName: "echarts" */ 'echarts').then(echarts => {
            let myChart = echarts.init(document.getElementById('main'));
            // 指定图表的配置项和数据
            let option = {
            	title: {
                    text: 'ECharts 入门示例'
                },
                tooltip: {},
                legend: {
                    data:['销量']
                },
                xAxis: {
                    data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
                },
                yAxis: {},
                series: [{
                    name: '销量',
                    type: 'bar',
                    data: [5, 20, 36, 10, 10, 20]
                }]
            };
            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);
        })
    }

    render() {

        return (
            <div ref='device' className='show_content'>
                <div className='left_side'>
                    <div className='left_top'>
                        <div className="card_header">快捷方式</div>
                        <div className="card_body">
                            <div className="body_list">
                                <ul className="layui-row layui-col-space10 layui-this">
                                    <li className="layui-col-xs3">
                                        <a lay-href="home/homepage1.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a lay-href="home/homepage2.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>主页二</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a lay-href="component/layer/list.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>弹层</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a layadmin-event="im">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>聊天</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a lay-href="component/progress/index.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>进度条</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a lay-href="app/workorder/list.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>工单</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a lay-href="user/user/list.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>用户</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a lay-href="set/system/website.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>设置</cite>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='right_top'>
                        <div className="card_header">快捷方式</div>
                        <div className="card_body">
                            <div className="body_list">
                                <ul className="layui-row layui-col-space10 layui-this">
                                    <li className="layui-col-xs3">
                                        <a lay-href="home/homepage1.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>主页一</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a lay-href="home/homepage2.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>主页二</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a lay-href="component/layer/list.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>弹层</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a layadmin-event="im">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>聊天</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a lay-href="component/progress/index.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>进度条</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a lay-href="app/workorder/list.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>工单</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a lay-href="user/user/list.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>用户</cite>
                                        </a>
                                    </li>
                                    <li className="layui-col-xs3">
                                        <a lay-href="set/system/website.html">
                                            <Icon type="calendar" className='small_icon'/>
                                            <cite>设置</cite>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="center_bottom">
                        <div className='layui-card'>
                            <div className="layui-card-header">数据概览</div>
                            <div className="layui-card-body">
                                <StackArea />
                            </div>
                        </div>
                    </div>
                    <div className="center_bottom">
                        <div className='layui-card'>
                            <div className="layui-card-header">数据概览</div>
                            <div className="layui-card-body">
                                <StackBar />
                            </div>
                        </div>
                    </div>
                </div>

                <div className='right_side'>
                    <div className="layui-card">
                        <div className="layui-card-header">版本信息</div>
                        <div className="layui-card-body layui-text">
                            <table className="layui-table">
                                <tbody>
                                    <tr>
                                        <td>当前版本</td>
                                        <td>
                                            <a>更新日志</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>基于框架</td>
                                        <td>
                                            layui-v2.4.5 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>主要特色</td>
                                        <td>零门槛 / 响应式 / 清爽 / 极简</td>
                                    </tr>
                                    <tr>
                                        <td>获取渠道</td>
                                        <td>
                                            <div className="layui-btn-container">
                                                <a href="http://www.layui.com/admin/" target="_blank" className="layui-btn layui-btn-danger">获取授权</a>
                                                <a href="http://fly.layui.com/download/layuiAdmin/" target="_blank" className="layui-btn">立即下载</a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div className="layui-card">
                        <div className="layui-card-header">效果报告</div>
                        <div className="layui-card-body layadmin-takerates">
                        	<div className="layui-progress">
                                <div className="layui-progress-bar">
                                    <span className="layui-progress-text">65%</span>
                                </div>
                                <div className="layui-progress" lay-showpercent="yes">
                                    <h3>签到率</h3>
                                    <div className="layui-progress-bar" lay-percent="32%">
                                        <span className="layui-progress-text">32%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="layui-card">
                        <div className="layui-card-header">效果报告</div>
                        <div className="layui-card-body layadmin-takerates">
                        	<div className="layui-progress">
                                <div className="layui-progress-bar">
                                    <span className="layui-progress-text">65%</span>
                                </div>
                                <div className="layui-progress" lay-showpercent="yes">
                                    <h3>签到率</h3>
                                    <div className="layui-progress-bar" lay-percent="32%">
                                        <span className="layui-progress-text">32%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="layui-card">
                        <CoordinateBar />
                    </div>
                    
                    <div className="layui-card">
                        <CoordinateStack />
                    </div>
                    
                </div>
            </div>
        )
    }

    componentDidMount(){
        const _self = this;
        //_self._init();
    }

    // 组件接收到新的props时调用,并将其作为参数nextProps使用
    componentWillReceiveProps(nextProps) {  // 接收新的参数
        //console.log('设备')
        //console.log(nextProps.data)
       
    }
    
    componentDidUpdate() {
        this._init()
    }

}

export default CarBar;


