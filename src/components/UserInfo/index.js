import React from 'react';
import ReactDOM from 'react-dom';
import CarBar from '../CarBar2';


/**
 * 作者：Abbott.liu
 * 时间：2018年2月21日
 * 描述：车联网 - 设备
 */

class CarList extends React.Component {
    constructor() {
        super();
        this.state = {
            name: [],
            quantity: []
        }
    }

    _init = () => {

        let dataStorage = window.localStorage;
        let userToken = dataStorage.getItem("userToken");
        if (!userToken) {
            return;
        }
        if (userToken.indexOf('obj-') === 0) {
            userToken = userToken.slice(4);
            userToken = JSON.parse(userToken);
        } else if (userToken.indexOf('str-') === 0) {
            userToken = userToken.slice(4);
        }

        console.log("userToken == ", userToken);

        fetch("http://118.24.248.134:8089/admin/user/get_user_info",{
            method:"POST",
            mode: 'cors',
            headers: {
                "Content-Type": "application/json",
                "token": userToken
            },
            body:JSON.stringify({
                "userId":12
            })
        }).then(
            response => {
                // console.log("response ==", response);
                return response.json();
            }
        ).then((data) => {
            console.log(data);
        });
    }

    render() {

        return (
            <div className={'coordinateStack'} ref='coordinateStack'>
                车辆管理列表
            </div>
        )
    }

    componentDidMount() {
        const _self = this;
        _self._init();

    }

}

export default CarList;
