import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.less';
import DefaultStyle from '../DefaultStyle';
import CarBar1 from '../CarBar1';
import CarBar2 from '../CarBar2';
import EditableTable from '../EditableTable';
import DeviceTable from '../DeviceTable';
import UserList from '../UserList';
import UserInfo from '../UserInfo';
import UserCarInfo from '../UserCarInfo';
import CarList from '../CarList';
import LogList from '../LogList';
import AdminList from '../AdminList';
import AddAdmin from '../AddAdmin';


/**
 * 作者：Abbott.liu
 * 时间：2018年2月21日
 * 描述：车联网 - 设备
 */

import { Tabs, Button } from 'antd';

const TabPane = Tabs.TabPane;

class TableMarker extends React.Component {
  constructor(props) {
    super(props);
    this.newTabIndex = 0;
    const panes = [
      { title: 'Tab1', content: 'Content of Tab Pane 1', key: '1' }
    ];
    this.state = {
      activeKey: panes[0].key,
      panes,
    };
    
  }

  onChange = (activeKey) => {
    const _self = this;
    console.log ('改变展示书签 === ', activeKey);
      _self.props.editTag( activeKey );
    //this.setState({ activeKey });
  }

  onEdit = (targetKey, action) => {
    this[action](targetKey);
  }

  add = () => {
    const panes = this.state.panes;
    const activeKey = `newTab${this.newTabIndex++}`;
    panes.push({ title: 'Tab3', content: 'New Tab Pane', key: activeKey });
    this.setState({ panes, activeKey });
  }

  remove = (targetKey) => {
      // 删除
      const _self = this;
      _self.props.removeTag( targetKey );
      
      /*
      let activeKey = this.state.activeKey;
      let lastIndex;
      console.log ( 'targetKey == ', targetKey);
      this.state.panes.forEach((pane, i) => {
        if (pane.key === targetKey) {
          lastIndex = i - 1;
        }
      });
      const panes = this.state.panes.filter(pane => pane.key !== targetKey);
      if (lastIndex >= 0 && activeKey === targetKey) {
        activeKey = panes[lastIndex].key;
      }
      console.log( 'panes == ', panes);
      this.setState({ 
        panes: panes, 
        activeKey: activeKey
      });
      */
  }

  _init = () => {
    const _self = this;
    //console.log( '_self.props == ', _self.props);
    _self.setState({ 
      panes: _self.props.componentNames.panes,
      activeKey: _self.props.componentNames.activeKey
    });
  }

  render() {
    const _self = this;
    //console.log( '_self.props == ', _self.props );
    let rightComponent = {
      'default-style': <DefaultStyle />,
      'car-manage': <CarBar1 />,
      'Tab3': <CarBar2 />,
      'device-manage': <DeviceTable />,
        "user-list": <UserList />,
        "user-info": <UserInfo />,
        "user-car-info": <UserCarInfo />,
        "car-list": <CarList />,
        "log-list": <LogList />,
        "admin-list": <AdminList />,
        "add-admin": <AddAdmin />

    };

    //console.log( 'component-panes == ', this.props.componentNames.panes );
    //console.log( 'activeKey == ', this.props.activeKey );

    console.log('panes == ', this.props.componentNames.panes);

    return (
      <div className={'tab_style'}>
        <Tabs
          hideAdd
          onChange={this.onChange}
          activeKey={this.props.activeKey}
          type="editable-card"
          onEdit={this.onEdit}
        >
          {
            this.props.componentNames.panes.map(pane => 
              <TabPane tab={pane.title} key={pane.key}>{rightComponent[pane.content]}</TabPane>)
          }
        </Tabs>
      </div>
    );
  }

  componentWillReceiveProps( newProps ) {
    console.log('Component WILL RECEIVE PROPS!');
    const _self = this;
    //console.log( '接收新的参数 newProps == ', _self.newProps);
  }

  shouldComponentUpdate (nextProps,nextState) {
    return true;
  }

  componentWillUpdate (nextProps,nextState) {
    //console.log('Component WILL UPDATE!');
    const _self = this;
    //console.log( '接收新的参数 nextProps == ', _self.nextProps);
    //console.log( '接收新的参数 nextState == ', _self.nextState);
  }

  componentDidUpdate (prevProps,prevState) {
    const _self = this;
    //_self._init();
  }
}

export default TableMarker;