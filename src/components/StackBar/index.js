import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.less'

/**
 * 作者：Abbott.liu
 * 时间：2018年2月21日
 * 描述：车联网 - 设备
 */

class StackBar extends React.Component {
    constructor() {
        super();
        this.state = {
            name: [],
            quantity: []
        }
    }

    _init() {
        // 参数设置
        const _self = this;
        return import(/* webpackChunkName: "echarts" */ 'echarts').then(echarts => {
            let myChart = echarts.init(_self.refs.stackBar);
            // 指定图表的配置项和数据
            let option = {
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                        type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                legend: {
                    data: ['直接访问', '邮件营销','联盟广告','视频广告','搜索引擎']
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis:  {
                    type: 'value'
                },
                yAxis: {
                    type: 'category',
                    data: ['周一','周二','周三','周四','周五','周六','周日']
                },
                series: [
                    {
                        name: '直接访问',
                        type: 'bar',
                        stack: '总量',
                        label: {
                            normal: {
                                show: true,
                                position: 'insideRight'
                            }
                        },
                        data: [320, 302, 301, 334, 390, 330, 320]
                    },
                    {
                        name: '邮件营销',
                        type: 'bar',
                        stack: '总量',
                        label: {
                            normal: {
                                show: true,
                                position: 'insideRight'
                            }
                        },
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: '联盟广告',
                        type: 'bar',
                        stack: '总量',
                        label: {
                            normal: {
                                show: true,
                                position: 'insideRight'
                            }
                        },
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: '视频广告',
                        type: 'bar',
                        stack: '总量',
                        label: {
                            normal: {
                                show: true,
                                position: 'insideRight'
                            }
                        },
                        data: [150, 212, 201, 154, 190, 330, 410]
                    },
                    {
                        name: '搜索引擎',
                        type: 'bar',
                        stack: '总量',
                        label: {
                            normal: {
                                show: true,
                                position: 'insideRight'
                            }
                        },
                        data: [820, 832, 901, 934, 1290, 1330, 1320]
                    }
                ]
            };
            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);
        })
    }

    render() {

        return (
            <div className={'stack_bar'} ref='stackBar'>
            123
            </div>
        )
    }

    componentDidMount(){
        const _self = this;
        _self._init();
    }

    // 组件接收到新的props时调用,并将其作为参数nextProps使用
    componentWillReceiveProps(nextProps) {  // 接收新的参数
        //console.log('设备')
        //console.log(nextProps.data)
       
    }
    
    componentDidUpdate() {
        this._init()
    }

}

export default StackBar;
