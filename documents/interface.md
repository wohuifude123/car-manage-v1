# 心里只有你生活网接口地址

> author: abbottliu

> date: 2018/5/13

## 登录

### 接口地址

```
http://39.106.161.34:3060/index/user/login/phone
```

### 参数列表



## 注册

### 接口地址

```
http://39.106.161.34:3060/user/register/phone
```

### 参数列表

```
{
	"phone": "",  
	"password": "123456",
	"username": "瑞瑞",
	"sex": "0",
	"qq": "",
	"weChat": "18610886337"
}
```

| Name | Description |
|---------|-------------|
| phone | 登陆账号 |
| password | 密码 |
| username | 用户姓名 |
|sex 性别| 密码 |
| qq | QQ号 |
| weChat | 微信号 |

### 成功结果

```
{
    "code": "0001",
    "function": "register-phone",
    "message": "success",
    "detail": "账号注册成功"
}
```

## 获取个人资料

## 获取用户列表

## 修改用户资料

```
http://127.0.0.1:3060/heart/user/information/update
```

```
{
	"name": "",
	"identity": "",
	"weChat": "",
	"email": "",
	"birthday": "",
	"sex": "",
	"phone": "111"
}
```


## 获取文章列表


## 修改文章详情

如果不是密码登录 用验证码登录的话 还需要一个发送验证码的接口