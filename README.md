# 心里只有你生活网后台管理系统
>author: abbott.liu

## 启动指令

|顺序|指令|功能|
|:----:|:----:|:----:|
|0| npm install   | 下载依赖包 |
|1| npm run dll   | 处理公共依赖，可按需修改第三方依赖 |
|2| npm run dev   | 开发环境 |
|3| npm run build | 开发环境 |


### 辅助指令

```sh
npm run zip 打包dist下代码到release.zip
```

### 打包

```sh

-c: 建立压缩档案
-x: 解压
-t: 查看内容
-r: 向压缩归档文件末尾追加文件
-u: 更新原压缩包中的文件
-z: 有gzip属性的
-j: 有bz2属性的
-Z: 有compress属性的
-v: 显示所有过程
-O: 将文件解开到标准输出

压缩

tar cvzf dist.tar.gz dist/

解压

tar -xzvf dist.tar.gz ./

连接

ssh root@111.231.243.58

scp /Users/abbott/Desktop/company/car-manage-v1/dist.tar.gz root@111.231.243.58:/usr/local/nginx/html/manage

```


>注意： 
>
>1、开发环境下由于`style-loader`引入样式滞后，导致一些问题，比如echarts图表未渲染成功，可以通过style直接设置`height/width`解决当前问题，当然在生产环境下将css单独打包并不会有问题
>
>2、使用HMR方式监控代码，修改index.html和src/static下静态被复制的文件无法实现热更新

## 其他说明文件

[接口地址](documents/interface.md)

## 文件目录结构

``` bash
├── build                         构建脚本目录
│   ├── build.js                     生产环境构建脚本
│   ├── check-versions.js
│   ├── utils.js                     构建相关工具方法
│   ├── vue-loader.conf.js
│   ├── webpack.base.conf.js         webpack基础配置
│   ├── webpack.dev.conf.js          webpack开发环境配置
│   └── webpack.prod.conf.js         webpack生产环境配置
├── config                        配置相关
│   ├── index.js
│   ├── dev.env.js                   开发环境变量
│   ├── prod.env.js                  生产环境变量
│   └── test.env.js
├── src
│   ├── components             // 全局UI组件
│   ├── pages                // 入口 加载组件 初始化等
│   │    └── home            //图表组件
│   │         ├── assets      资源文件
│   │         ├── modules     存放不同页面
│   │         ├── router                 // 路由
│   │         ├── main.js     // 入口 加载组件 初始化等
│   │         ├── App.vue                // 入口页面
│   │         ├── index.html           // html打包所用模板
│   ├── store                  // 全局store管理
│   ├── utils                  // 全局公用方法
├── lib                     // 第三方资源
├── .babelrc                   // babel-loader 配置
├── eslintrc.js                // eslint 配置项
├── .gitignore                 // git 忽略项
└── package.json               // package.json
```
